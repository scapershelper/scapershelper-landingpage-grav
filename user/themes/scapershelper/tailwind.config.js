
const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
    content: ['./templates/**/*.html.twig', '../../pages/**/*.md'],
    options: {
      safelist: ['notices'],
    }
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        gray: colors.trueGray,
        primary: '#97b944',
        secondary: '#C4B248',
        third: '#39826A',
      },
    },
  },
  variants: {
    extend: {
      margin: ['last'],
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
