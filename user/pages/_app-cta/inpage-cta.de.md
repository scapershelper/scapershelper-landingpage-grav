---
title: 'Hol dir die Scaper''s Helper App!'
body_classes: modular
---

* Das 90-Tage Programm unterstützt dich beim Einfahren deines Aquascapes
* Lege Logbucheinträge mit Text und Wasserwerten an
* Lege tägliche, wöchentliche oder monatliche Erinnerungen fest
* Lerne mehr über Aquascaping und Aquarien mit unseren Artikeln
* Keine Registrierung oder Angabe persönlicher Daten notwendig