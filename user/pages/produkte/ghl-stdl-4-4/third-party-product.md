---
title: GHL Powerbar STDL-4-4
summary: Die GHL STDL-4-4 ist eine steuerbare Vierfachsteckdose. Mit ihr lassen sich
  beispielsweise die CO<sub>2</sub>-Zufuhr, der Filter oder der Heizer wunderbar schalten.
compatibility:
  compatible: true
  limitations: false
  port: TODO
price: 130
---

# GHL Powerbar STDL-4-4



## Kompatibilität mit dem Scaper's Helper
Kompatibel ohne Einschränkungen

## Anschluss an den Scaper's Helper
An den TODO-Port

## Bilder

## Technische Details

### Technische Daten

| Parameter           | Wert      | Einheit |
|---------------------|----------:|--------|
| Spannung            | 12        | V  |
| Stromaufnahme/Kanal |  | mA |

### Steckerbelegung