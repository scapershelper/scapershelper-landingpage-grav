---
title: Jebao und Jecod DP3S und DP4S
summary: Die günstigen Dosierer von Jebao bzw. Jecod sind zwar nicht hochpräzise, 
  aber sehr gut geeignet für die Bedürfnisse eines Aquascapes.
compatibility:
  compatible: true
  limitations: true
  port: TODO
price: 40
---

# Jebao und Jecod DP3S und DP4S

![](doser1.jpg?classes=rounded,m-2,sm:w-1/2,mx-auto,float-right)

Bei den Dosierern DP3S und DP4S, die unter den Firmennamen *Jebao* und *Jecod* 
vertrieben werden, handelt es sich um 3- beziehungsweise 4-Kanal Dosierer.
Sie dienen als Erweiterung zu den von Jebo bzw. Jecod vertriebenen Dosierern
mit integrierter Steuerung. 

Die DP3S und DP4S sind "Slave"-Geräte. Sie haben also keine integrierte Steuerung.
Die Dosierer werden über einen 6-poligen RJ-Stecker mit der Steuerungseinheit 
verbunden. 

## Kompatibilität mit dem Scaper's Helper
Kompatibel mit der Einschränkung, dass die Kanal-LEDs nicht funktionstüchtig sind.

## Anschluss an den Scaper's Helper
An den TODO-Port.

## Bilder

![](doser1.jpg?classes=rounded,m-2,w-2/3,mx-auto)
![](doser2.jpg?classes=rounded,m-2,w-2/3,mx-auto)

## Technische Details

### Technische Daten

| Parameter            | Wert      | Einheit |
|----------------------|----------:|--------|
| Nennspannungspannung | 12        | V  |
| Stromaufnahme/Kanal  | 110 - 170 | mA |

- Die Spannung kann bis auf ca. 6 V reduziert werden um eine geringere 
  Fördermenge zu erreichen
- Ab unter ca. 5 V wird es langsam holprig
- Die Pumpen können auch verpolt betreiben werden. Dann leuchten die LEDs
  aber nicht auf sobald eine Pumpe läuft.

### Steckerbelegung

![Steckerbelegung](rj11-female-pin-assignment.svg)

| Pin | Belegung  |
|-----|-----------|
| 1   | Gemeinsame Anode |
| 2   | Kanal P8 |
| 3   | Kanal P7 |
| 4   | Kanal P6 |
| 5   | - |
| 6   | Gemeinsame Anode |
