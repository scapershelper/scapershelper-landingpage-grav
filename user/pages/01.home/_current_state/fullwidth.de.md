---
title: 'Aktueller Stand'
anchor: current_status
---

Der Scaper's Helper befindet sich noch in der Entwicklung. Der zweite Prototyp ist kürzlich 
fertig geworden und wird derzeit getestet. Nachdem alle Tests abgeschlossen sind,
soll eine Crowdfunding-Kampagne gestartet werden, um die hohen initialen Zertifizierungs-
und Produktionskosten zu decken. [Bestelle vor](/bestellen), um keine Neuigkeiten zu verpassen. Gerne kannst du auch deine eigenen Ideen einbringen!

![Der Scaper's Helper](steuerung_sm.webp?resize=900.900)

Die nächsten Entwicklungsschritte umfassen:

- Den Hardware- sowie die App-Prototyp so weit fertig zu stellen, dass sie von anderen getestet werden kann
- Zuberhör-Hersteller anfragen, um deren Produkte auf Kompatibilität zu testen
- Das Projekt bekannter machen und eine rege Community entwickeln 
- Die Crowdfunding-Kampagne vorbereiten

Willst Du nichts verpassen oder deine eigene Ideen einbringen? [Dann bestelle jetzt vor!](/bestellen).