---
title: 'Current Project State'
anchor: current_status
---

The Scaper's Helper is still under development. The second prototype is recent
finished and is currently being tested. After all tests have been completed,
a crowdfunding campaign is to be started in order to raise the high initial certification
and to cover production costs. [Pre-order](/bestellen) so as not to miss any news. You are also welcome to contribute your own ideas!

![The Scaper's Helper](steuerung_sm.webp?resize=900.900)

The next development steps include:

- To complete the hardware and the app prototype so that they can be tested by others
- Inquire about accessory manufacturers to test their products for compatibility
- Make the project better known and develop a lively community
- Prepare the crowdfunding campaign

Don't miss anything and bring in your own ideas: [Preorder now!](/bestellen).