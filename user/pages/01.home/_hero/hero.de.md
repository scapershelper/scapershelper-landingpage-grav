---
title: Hero
button_more:
    text: 'Mehr erfahren'
    url: '#checklist'
button_buy:
    text: Vorbestellen
    url: /bestellen
---

<!-- <h1 class="text-2xl mb-4">Du vergisst tägliches Düngen regelmäßig? Schlechtes Pflanzenwachstum? Zu viele Algen?</h1> -->


<h1 class="text-2xl font-medium mb-4">
Perfektes Aquascape, minimaler Aufwand.
</h1>

<p class="text-lg m-2 mb-4 font-light">
Der Scaper's Helper ist die neue Aquascape-Steuerung für erfolgreiches Pflanzenwachstum 
und perfekte Beleuchtung. Er dimmt LEDs, dosiert Dünger, schaltet Steckdosen und überwacht
Temperatur, Wasserstand, uvm.
</p>

<p class="text-lg font-bold m-2">
Crowdfunding-Start in wenigen Wochen!
</p>
