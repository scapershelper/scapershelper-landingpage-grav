---
title: Hero
button_more:
    text: 'Learn more'
    url: '#checklist'
button_buy:
    text: Preorder
    url: /bestellen
---

<h1 class="text-2xl mb-6">
Perfect scape, minimal effort.
</h1>

<p class="text-lg m-2 font-light">
The Scaper's Helper is the new Aquascape controller for successful plant growth and
perfect lighting. It dims LEDs, doses fertilizer, switches sockets and monitors parameters.
</p>

<p class="text-lg font-bold m-2">
Crowdfunding starts in a few weeks!
</p>
