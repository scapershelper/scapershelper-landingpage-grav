---
title: app-teaser
subtitle: Hol dir die App auf Google Play!
---

* Das 90-Tage Programm unterstützt dich beim Einfahren deines Aquascapes
* Lege Logbucheinträge mit Text und Wasserwerten an
* Lege tägliche, wöchentliche oder monatliche Erinnerungen fest
* Lerne mehr über Aquascaping und Aquarien mit unseren gut recherchierten Artikeln
* Die App ist unabhängig von der Steuerung nutzbar
* Kostenlos, keine Werbung und keine Registrierung notwendig
