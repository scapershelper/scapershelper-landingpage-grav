---
title: app-teaser
subtitle: Get the app on Google Play!
---

* The 90-day program supports you in starting your aquascape
* Create logbook entries with text and water values
* Set daily, weekly or monthly reminders
* Learn more about aquascaping and aquariums with our well researched articles
* The app is independent from the controller
* Free, no ads and no registration necessary
