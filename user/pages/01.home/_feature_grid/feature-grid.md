---
title: 'Funktionen im Detail'
anchor: details
features:
    -
        title: 'Dimmen von LED Leuchtbalken'
        subtitle: 'Sonnenauf- und Untergang, Mondphasen etc.'
        icon: lightbulb-on
        text: 'Dimme LED Leuchten mit Sonnenauf- und Untergang, Mondlicht, Gewitter- und Wolkensimulation. So erscheinen deine Pflanzen in perfektem Licht.'
    -
        title: 'Dünger dosieren'
        subtitle: 'Perfektes Pflanzenwachstum'
        icon: tint
        text: 'Mit absoluter Genauigkeit und beliebig vielen Dosierungen am Tag erreichst du perfektes Pflanzenwachstum.'
    -
        title: 'Steckdosen schalten'
        subtitle: 'Wartung vereinfachen und CO<sub>2</sub> sparen'
        icon: plug
        text: 'Filter, Heizer und CO<sub>2</sub> werden ausgeschaltet, während du dein Scape wartest. Spare CO<sub>2</sub> mit der Nachtabschaltung deiner Druckgas-Anlage.'
    -
        title: Voice-Assistant-Integration
        subtitle: 'Hände nass? Kein Problem!'
        icon: microphone
        text: 'Sind die Hände im Aquarium, kannst du den Scaper''s Helper über Google Home oder Alexa steuern. Das ist natürlich kein Muss. Der Scaper''s Helper funktioniert auch ohne Internetzugriff.'
    -
        title: 'Messen und Regeln'
        subtitle: 'Alle Parameter stets im Blick'
        icon: thermometer-half
        text: 'Mit Anschlussmöglichkeiten für Temperatursensor, pH-Sonde, Durchflussmesser, Pegelsensor und mehr, hast du alle wichtigen Parameter rund um dein Scape immer im Blick.'
    -
        title: Alarme
        subtitle: 'Temperatur zu hoch/niedrig? Wartung fällig?'
        icon: exclamation-circle
        text: 'Alarme machen dich bei ungünstigen Messwerten aufmerksam, warnen dich bei austretendem Wasser oder zu niedrigem Wasserstand. Außerdem wirst du an regelmäßig anstehende Termine, wie z.B. wöchentliche Wartung, erinnert.'
    -
        title: Datenlogging
        subtitle: 'Daten aufzeichnen und analysieren'
        icon: wifi
        text: 'Zeichne alle Messdaten und wichtigen Ereignisse auf und analysiere sie später in unserem Online-Portal.'
    -
        title: 'Einfache Bedienbarkeit'
        subtitle: 'Moderne, übersichtliche Oberflächen'
        icon: smile-beam
        text: 'Über eine moderne App lässt dich der Scaper''s Helper spielend leicht konfigurieren.'
    -
        title: DIY-freundlich
        subtitle: 'Deine Software- und Hardware-Erweiterungen'
        icon: microchip
        text: 'Durch eine ausführliche Dokumentation von Soft- und Hardware kannst Du spielend leicht deine eigens gebauten Erweiterungen anschließen.'
    -
        title: 'Deine Idee'
        highlight: true
        subtitle: 'Bringe deine Idee ein!'
        icon: rocket
        text: "Der Scaper's Helper ist ein community-getriebenes Projekt. Bringe deine eigenen Ideen mit ein für deine perfekte Aquascape-Steuerung.\n\nMach mit auf [Facebook](https://facebook.com/groups/ScapersHelper) oder im [Forum](https://forum.scapershelper.de/)!"
---

