---
title: 'Function Details'
anchor: details
features:
    -
        title: 'Dimming of LED bars'
        subtitle: 'Sunrise, sunset and moon phase simulation.'
        icon: lightbulb-on
        text: 'Dim LED lights with sunrise and sunset, moonlight, thunderstorm and cloud simulation. Your scape will appear in perfect light.'
    -
        title: 'Fertilizer dosing'
        subtitle: 'Perfect plant growth'
        icon: tint
        text: 'Witht he 3 built in dosing pumps, you''ll achieve perfect plant growth with absolute accuracy and any number of doses per day.'
    -
        title: 'Switch sockets'
        subtitle: 'Ease your maintenance and save CO<sub>2</sub>'
        icon: plug
        text: 'The filter, heater and CO<sub>2</sub> will be switched off while you are waiting for your scape. Save CO<sub>2</sub> by switching off your pressurized gas system overnight.'
    -
        title: Voice-Assistant-integration
        subtitle: 'Hands wet? No Problem!'
        icon: microphone
        text: 'If your hands are in the aquarium, you can control the Scaper''s Helper via Google Home or Alexa. This is of course not a must. The Scaper''s Helper also works without internet access.'
    -
        title: 'Monitor and Control'
        subtitle: 'All parameters always in view'
        icon: thermometer-half
        text: 'With connection options for temperature sensors, pH probes, flow meters, level sensors and more, you always have an overview of all the important parameters relating to your scape.'
    -
        title: Alarms
        subtitle: 'Temperature too high or low? Maintenance due?'
        icon: exclamation-circle
        text: 'Alarms draw your attention to unfavorable readings and warn you if water is leaking or the water level is too low. You will also be reminded of regular upcoming appointments, such as weekly maintenance.'
    -
        title: Data-Logging
        subtitle: 'Collect data and analyze'
        icon: wifi
        text: 'Record all measurement data and important events and analyze them later in our online portal.'
    -
        title: 'Ease of use'
        subtitle: 'Modern and easy-to-use App'
        icon: smile-beam
        text: 'The Scaper''s Helper is child''s play to configure using a modern app.'
    -
        title: DIY-friendly
        subtitle: 'Your own soft- and hardware addons'
        icon: microchip
        text: 'With detailed documentation of software and hardware, you can easily connect your DIY extensions.'
    -
        title: 'Your Idea!'
        highlight: true
        subtitle: 'Bring in your idea!'
        icon: rocket
        text: "The Scaper's Helper is a community-driven project. Bring your own ideas for your perfect Aquascape control.\n\nJoin us on [Facebook](https://facebook.com/groups/ScapersHelper)"
---

