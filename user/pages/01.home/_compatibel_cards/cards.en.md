---
title: 'Compatible products'
anchor: compatible
features:
    -
        title: 'LED bars'
        image: led.webp
        text: 'LED lights from Chihiros, Kessil, Aqua-Grow [and other manufacturers](/knowledge/technical-details#compatible-led-lights) can be dimmed with the Scaper''s Helper. Adapters will be available in the shop later.'
    -
        title: pH-Probes
        image: ph_sonde_bearbeitet_sm.webp
        text: 'You can monitor the pH value using commercially available pH probes. Our own probes will be offered in the shop later.'
    -
        title: Flow-Sensors
        image: durchflusssensor_sm.webp
        text: 'The flow sensors available online are compatible. Different variants with pre-assembled cables and plugs will be offered in the shop later.'
    -
        title: Dosers
        text: 'The slave dispensers from JECOD and AquaMedic are compatible with the Scaper''s Helper. Specially developed dispensers with more functions are in the planning stage.'
    -
        title: 'Switched socket GHL STDL 4-4'
        text: 'The four-way socket strip STDL 4-4 sold by GHL is compatible with the Scaper''s Helper. A specially developed socket strip with more functions is in the planning stage.'
    -
        title: 'Humidity Sensors'
        text: 'DHT11, DHT22 and BME280 humidity sensors are compatible. Adapters and a variant with a sturdy housing and pre-assembled cable will be offered in the shop later.'
    -
        title: 'Temperature Sensors'
        image: ds18b20_bearbeitet.webp
        text: 'DS18B20 temperature sensors are compatible. A watertight version with a robust housing and pre-assembled cable will be offered in the shop later.'
    -
        title: 'Water Level Sensors'
        image: pegelsensoren.webp
        text: 'Mechanical and optical level sensors are compatible and will later also be offered in the shop.'
    -
        title: 'Leakage Sensors'
        image: leakage_sm.webp
        text: 'Leak sensors such as those offered on eBay, for example, are compatible. A watertight version with a robust housing and pre-assembled cable will be offered in the shop later.'
    -
        title: 'Solenoid Valves'
        image: magnetventil.webp
        text: '12 V solenoid valves can be connected directly to the Scaper''s Helper. Compatible products will later also be offered in the shop.'
    -
        title: 'Wave Pumps'
        text: 'Wave pumps with 0-10 V input can be operated on the Scaper''s Helper. Adapters will be offered in the shop later.'
    -
        title: 'Much more'
        text: 'On [our compatibility page](/knowledge/technical-details) you will find all details about compatible products and the connections of the Scaper''s Helper.'
---

