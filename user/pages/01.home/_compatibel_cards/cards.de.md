---
title: 'Kompatible Produkte'
anchor: compatible
features:
    -
        title: 'LED Leuchten'
        image: led.webp
        text: 'LED Leuchten von Chihiros, Kessil, Aqua-Grow [und weiteren Herstellern](/wissen/technische-details#kompatible-led-leuchten) sind mit dem Scaper''s Helper dimmbar. Adapter werden später im Shop angeboten.'
    -
        title: pH-Sonden
        image: ph_sonde_bearbeitet_sm.webp
        text: 'Mit handelsüblichen pH-Sonden kannst du den pH-Wert überwachen. Eigene Sonden werden später im Shop angeboten.'
    -
        title: Durchflusssensoren
        image: durchflusssensor_sm.webp
        text: 'Die im Onlinehandel befindlichen Durchflusssensoren sind kompatibel. Verschiedene Varianten mit vorkonfektionierten Kabeln und Steckern werden später im Shop angeboten.'
    -
        title: Dosierer
        text: 'Die Slave-Dosierer der Firma JECOD und AquaMedic sind mit dem Scaper''s Helper kompatibel. Eigens enwtickelte Dosierer mit mehr Funktionen sind in Planung.'
    -
        title: 'Steckdosenleiste GHL STDL 4-4'
        text: 'Die von GHL vertriebene Vierfachsteckdosenleiste STDL 4-4 ist mit dem Scaper''s Helper kompatibel. Eine eigens entwickelte Steckdosenleiste mit mehr Funktionen ist in Planung.'
    -
        title: Feuchtigkeitssensoren
        text: 'Feuchtigkeitssensoren der Typen DHT11, DHT22 und BME280 sind kompatibel. Adapter und eine Variante mit robustem Gehäuse und konfektioniertem Kabel wird später im Shop angeboten.'
    -
        title: Temperatursensoren
        image: ds18b20_bearbeitet.webp
        text: 'Temperatursensoren des Types DS18B20 sind kompatibel. Eine wasserdichte Variante mit robustem Gehäuse und konfektioniertem Kabel wird später im Shop angeboten.'
    -
        title: Pegelsensoren
        image: pegelsensoren.webp
        text: 'Mechanische und optische Pegelsensoren sind kompatibel und werden später auch im Shop angeboten.'
    -
        title: Lecksensoren
        image: leakage_sm.webp
        text: 'Lecksensoren wie sie beispielsweise auf eBay angeboten werden sind kompatibel. Eine wasserdichte Variante mit robustem Gehäuse und konfektioniertem Kabel wird später im Shop angeboten.'
    -
        title: Magnetventile
        image: magnetventil.webp
        text: '12 V Magnetventile sind direkt an den Scaper''s Helper anschließbar. Kompatible Produkte werden später auch im Shop angeboten.'
    -
        title: Strömungspumpen
        text: 'Strömungspumpen mit 0-10 V Eingang können am Scaper''s Helper betrieben werden. Adapter werden später im Shop angeboten.'
    -
        title: 'Vieles Mehr'
        text: 'Auf [unserer Kompatibilitäts-Seite](/wissen/technische-details) findest du alle Details zu kompatiblen Produkten und den Anschlüssen des Scaper''s Helper.'
---

