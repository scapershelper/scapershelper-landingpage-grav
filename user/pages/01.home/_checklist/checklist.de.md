---
title: 'Mach dein Scape smart'
anchor: checklist
checklist:
    - 'LED-Leuchtbalken dimmen'
    - 'Automatisiert Düngen'
    - 'Einfahrassistent als Starthilfe für dein Scape'
    - 'Messung von Temperatur, pH-Wert und Wasserstand'
    - 'Kompatibel mit vielen existierenden Produkten'
    - 'Sprachsteuerung mit Google Home oder Alexa'
    - 'Steuerung und Statusüberwachung per App'
    - 'DIY-freundlich und offen für deine Erweiterungen'
---

Beim Scaper's Helper handelt es sich um eine Aquariensteuerung die in wenigen
Wochen mit einer Crowdfunding-Kampagne finanziert werden soll.

Der Scaper‘s Helper verhilft deinem Aquascape zum **idealen Pflanzenwachstum bei minimalem Aufwand**.
Starte mit einem **Rezept**, das die Grundeinstellungen für dein Aquascape vorgibt.
Der Scaper‘s Helper erledigt dann die regelmäßige **Düngung mittels integrierter Dosierpumpen**.
Perfekte Lichverhältnisse werden durch **Dimmung von RGB(W)-Leuchtbalken** inklusive Gewitter-,
Wolken- und Mondsimulation geschaffen. 

Komfort bei Wartung und CO<sub>2</sub>-Einsparung erhältst du durch **Schaltsteckdosen**.
Mit **Temperatursensoren** und **Pegelstandssensoren** können die Bedingungen im
und um das Aquascape überwacht werden.
Über eine **pH-Sonde** und CO<sub>2</sub>-Steuerung kann der **pH-Wert reguliert** werden.
Darüber hinaus kann die Steuerung mit den verfügbaren **<abbr title="Eingabe/Ausgabe">IO</abbr> und <abbr title="Pulsweitenmodulation">PWM</abbr> Ports** nach Belieben erweitert werden.