---
title: 'Make your scape smart'
anchor: checklist
checklist:
    - 'Dim LED bars'
    - 'Automatically fertilize 3+ fertilizers'
    - 'Assistant for starting up your scape'
    - 'Measure temperature, pH, water level and more'
    - 'Compatible with existing products'
    - 'Voice assitant integration'
    - 'Monitoring and control via our modern app'
    - 'DIY-friendly and open for your expansions'
---

The Scaper's Helper is an aquarium controller for which the crowdfunding will start in a few weeks.

The Scaper‘s Helper helps your aquascape to **ideal plant growth with minimal effort**.
Start with a ** recipe ** that specifies the basic settings for your aquascape.
The Scaper‘s Helper then takes care of the regular **fertilization using integrated dosing pumps**.
Perfect lighting conditions are achieved by **dimming RGB(W) light bars** including thunderstorms,
Cloud and moon simulation created.

Make maintenance less stressful and save CO<sub>2</sub> with **switchable sockets**.
The conditions in and around the Aquascape can be monitored with
**temperature and humidity sensors** and **water level sensors**.
The **pH value** can be regulated via a **pH probe** and CO<sub>2</sub>-solenoid.
If that's not enough for you, the controller can be expanded with your own modules as required with the available 
**<abbr title="Input / Output">IO</abbr> and <abbr title="Pulse width modulation">PWM</abbr> ports**.