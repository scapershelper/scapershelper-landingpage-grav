---
title: Preorder
slug: order
visible: true
---

We'll inform you as soon as the crowdfunding starts and you can preorder.
Don't miss anything and subscribe to the newsletter.

**Of course you can look forward to a huge discount at the start of sales.**

---

Until then, you are very welcome to contribute to the development of the Scaper's Helper.
Let us know about the features you want or which products you would like
would use with the controller. Let's discuss!