---
title: Vorbestellen
visible: true
---

Wir informieren dich, sobald das Crowdfunding startet und du vorbestellen kannst.
Abonniere den Newsletter und verpasse nichts.

**Natürlich darfst Du dich auf einen satten Rabatt beim Verkaufsstart freuen.**

---

Bis dahin kannst du dich sehr gerne in die Entwicklung des Scaper's Helper mit einbringen.
Lass uns von deinen Wunsch-Funktionen wissen oder davon, welche Produkte du gerne
mit der Steuerung benutzen würdest. Wir setzen alles daran, die beste Steuerung
für dich zu entwickeln.
