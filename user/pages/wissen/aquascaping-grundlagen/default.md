---
title: Aquascaping Grundlagen
summary: Hier erfährst du, wie du dein Aquascape erfolgreich startest. Von der Auswahl des Aquariums über die Auswahl des Bodengrundes, der Pflanzen, des Düngers, bis hin zum Besatz.
---

# Aquascaping Grundlagen
