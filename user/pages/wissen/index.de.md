---
title: Wissen
visible: false

content:
    items: '@self.children'
    pagination: ''
    limit: 15
    order:
        by: title
        dir: desc
    url_taxonomy_filters: true
---
