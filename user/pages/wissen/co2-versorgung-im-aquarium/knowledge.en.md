---
title: 'CO2 supply in the aquarium and aquascape'
slug: co2-in-the-aquarium-and-aquascape
card:
    text: 'CO2 supply is of fundamental importance in heavily planted aquariums. Find out everything you need to know about CO<sub>2</sub> here'
    image: title.webp
metadata:
    description: 'CO2 is fundamental importance for healthy plant growth. Read here what you need to know about CO2 in aquascape and aquarium.'
    'og:description': 'CO2 is fundamental importance for healthy plant growth. Read here what you need to know about CO2 in aquascape and aquarium.'
    'og:image': 'https://scapershelper.de/user/pages/wissen/co2/sharepic.webp'
page-toc:
    active: true
intro: 'A CO<sub>2</sub> supply is an elementary component of a heavily planted aquarium. It ensures healthy plant growth and thus helps prevent algae. Read on to learn what types of CO<sub>2</sub> systems there are, how you introduce CO<sub>2</sub> into the aquarium and many more details.'
---

# What is CO<sub>2</sub>?

Carbon is a basic building block of all living beings, including plants, of course.
Plants absorb carbon in the form of CO<sub>2</sub>, i.e. carbon dioxide.
CO<sub>2</sub> is therefore a decisive factor for healthy plant growth.

In addition to CO<sub>2</sub>, light and micro-
as well as macronutrients to the basic requirements of plant growth.
If these "ingredients" are in the right proportion, the plant does
[Photosynthesis](https://de.wikipedia.org/wiki/Photosynthesis) and builds the
organic matter required for growth.

If you want to keep demanding plants with a lot of light, CO<sub>2</sub> fertilization is strongly
recommended. **With the CO<sub>2</sub> supply you ensure that your plants do not
get symptoms of deficiency, grow healthily and prevent algae from spreading.**

However, CO<sub>2</sub> addition is not always necessary. In aquariums with less demanding plants
and low to medium-strong lighting, a CO<sub>2</sub> is not needed.
If you decide to set up a such a low tech tank, 
[look here for a detailed shopping list](/wissen/low-budget-einkaufsliste) which get's you started.

If you're interested in how plants grow with and without CO<sub>2</sub>, 
[this video](https://youtu.be/tecnHZdBbyE?t=78) shows the development of equally planted 
Aquariums with and without the addition of CO<sub>2</sub>.


# Overview: The different CO<sub>2</sub> systems

There are two established types of CO<sub>2</sub> supply for your tank. With a
**Bio-CO<sub>2</sub> system** the CO<sub>2</sub> is produced by fermenting yeast in a sugary solution.
Another possibility is a **pressurized gas system**. Here the CO<sub>2</sub> is produced industrially
and comes under high pressure in a gas cylinder.
Another, not yet widely used option is the **"Chemical CO<sub>2</sub> system"**.
The CO<sub>2</sub> is produced on site through the reaction of soda and citric acid.
<!-- TODO: 1 sentence with details -->

The difference between the various systems is explained below.

<!-- ## Bio-CO<sub>2</sub>
TODO

Night shutdown not possible

!!!! ** Pressurized gas CO<sub>2</sub> shopping list **
!!!! * Pressurized gas bottle
!!!! * Pressure reducer
!!!! * Needle valve
!!!! * (Optional) solenoid valve and timer
-->


## The pressurized gas CO<sub>2</sub> system

A pressurized gas system is usually the combination of a **pressurized gas CO<sub>2</sub> bottle**
in sizes from 500 g, 2 kg up to 10 kg, a **pressure reducer** and a **needle valve**.
The CO<sub>2</sub> in the bottle is under high pressure under which it is in liquid form.
If you let the CO<sub>2</sub> escape from the bottle it can expand and transitions to the gaseous state.

![CO2 bottle with pressure reducer, bottle pressure and working pressure barometer, needle valve and solenoid valve](co2flasche_nummeriert.webp?classes=caption "(1) bottle valve, (2) working pressure, (3) cylinder pressure, (4) working pressure adjustment wheel, (5) Needle valve, (6) solenoid valve ")

This high pressure makes it impossible to connect the bottle directly to a hose and
to connect the CO<sub>2</sub> to the aquarium via a diffuser or reactor (more on this below).
A necessary element of the pressurized gas system is therefore a **pressure reducer**.
It ensures that the high cylinder pressure of over 50 bar is brought to a so-called working pressure
is regulated down by approx. 2 bar.

However, the pressure reducer itself is still too imprecise to generate the required amount of CO<sub>2</sub>
that is to be continuously fed to the aquarium. Therefore
a **needle valve** is attached after the pressure reducer. It's through with this one
An adjusting screw is possible to precisely adjust the amount of CO<sub>2</sub> introduced.

### Night shut-off valve

Since the plants only do photosynthesis when sufficient light is available,
it makes sense to use CO<sub>2</sub> pressurized gas systems with a **night shutdown valve**.
This is an electrically switchable valve, a so-called solenoid valve,
which is usually attached directly after the pressure reducer. 
In the picture above it has the number 6.

!! **Attention:** A night shut-off valve does not replace the check valve.
!! Solenoid valve, needle valve and pressure reducer are designed for only carrying gases.
!! Water can destroy them.

The valve is only opened with a timer if the lighting is also switched on.
Alternatively, the valve can be switched on 30 minutes to an hour before the lighting
and 30 minutes to an hour before the lights are switched off.
This ensures that CO<sub>2</sub> is present exactly when the plants need it.

If the valve switches is switched on for eight hours, the CO<sub>2</sub> bottle
will last three times as long as without night shutdown.

<!-- Which bottle size for which basin size? -->

### Pressurized CO<sub>2</sub>: Component summary

Below find a list of components which are needed or optional for a pressurized system.

!!! **Compressed gas CO<sub>2</sub> components summarized**
!!! * **Pressurized gas cylinder**
!!! * **Pressure reducer** - regulates the cylinder pressure to the working pressure
!!! * **Needle valve** - For fine adjustment of the CO<sub>2</sub> amount fed into the aquarium
!!! * **Bubble counter** - To help you adjust the amount of CO<sub>2</sub> introduced
!!! * **Check valve** - To protect the valves and pressure reducers
!!! * (Optional) **Solenoid valve and timer** - To save CO<sub>2</sub>

### Where to fill the empty CO<sub>2</sub> bottle?

If the CO<sub>2</sub> bottle is empty, it must be refilled. This can usually
happened in three places: the gas trade, the beverage trade or the pet shop. The **gas trade**
probably has the best offer in terms of price. However, it is not uncommon
that your bottle will be exchanged for another, already filled bottle and the empty 
Bottle will remain with the dealer. It will be refilled there and returned to the cycle.
The advantage is that you always get a well maintained bottle.
But it may be that you get a bottle with different dimensions and
thus there may be space problems in the base cabinet.

Another option is **beverage retailers**. Many beverage retailers have the option
to fill your bottle. Here it is rather unusual that your bottle against a
other is exchanged. So you usually go with your own, freshly filled one
Bottle home.

You can also have your bottle filled at the **pet shop**. Here you always get your own bottle
filled up. The filling will probably be the most expensive in the pet shop.
As an example: A filling of my 2 kg CO<sub>2</sub> bottle at the local pet shop costs € 17.99.

Be sure to check for leaks after changing the bottle. You can do that with, for example
special leak detection spray or simply with washing-up liquid. Mix a small amount of water
with a little washing-up liquid. With your finger or a syringe you can now apply the mixture to the
screw connections. If you see it foaming or bubbling somewhere you have a leak and should
double check the screw connection and tighten it if necessary.

### Use of Sodastream CO<sub>2</sub> bottles at the aquarium

Sodastream bottles can usually be used via commercially available adapters.
The adapters can be found in online shops, for example on eBay or Amazon.

The use of sodastream CO<sub>2</sub> bottles offers the advantage that you
You can exchange empty bottles for full ones in many supermarkets and drugstores.
For most people, the way to the supermarket is probably much shorter than the one
to the nearest pet shop or gas dealer. A clear gain in comfort.

The soda stream bottles only contain 425 g of CO<sub>2</sub> however. The costs
Compared to filling a larger bottle at a gas or pet store are significantly higher.

## Chemical CO<sub>2</sub> system </sub>

A “chemical CO<sub>2</sub> system” is based on the reaction of
Soda and citric acid substances interact with each other.

The system consists of a stainless steel cylinder and a
pressure reducer, a needle valve, possibly a solenoid valve and
mostly a bubble counter. The chemicals can be filled in via unscrewing the pressure reducer.

!! Please adhere to the manual regarding the quantities of chemicals and the order
!! in which you fill them into the cylinder.

The chemicals sodium, citric acid and water are mixed in the stainless steel cylinder
usually in a ratio of 1: 1: 2. The reaction starts immediately and it becomes CO<sub>2</sub>
released. Now the pressure reducer together with the needle valve and any
Night shut-off screwed onto the cylinder.

The advantage over Bio-CO<sub>2</sub> is that this reaction runs continuously
and is not temperature dependent. The reaction is also independent of the pressure in the container.
This means that **night shutdown is possible**.

For a 30-40 liter tank 400 g each of baking soda and citric acid should last about 100 days 
[according to this video from AquaOwner](https://www.youtube.com/watch?v=8Wqjlauat0s).
If you buy baking soda and citric acid in quantities of one kilogram,
the cost of one filling is around 8 euros. A real alternative to the pressurized gas system.

<!-- TODO: Looks good! -->

<!-- Summary with table -->

# Introduce CO<sub>2</sub> into the aquarium

The aim of a CO<sub>2</sub> system is to dissolve the CO<sub>2</sub> in the aquarium water
to make it available to the plants.
To make this as efficient as possible, the area 
of the CO<sub>2</sub> should be as large as possible when it is led past the water. This can
can be achieved, for example, by “atomizing” the CO<sub>2</sub> into the smallest possible bubbles.

The methods presented below all have their advantages and disadvantages in terms of optics
or the effectiveness with which the CO<sub>2</sub> is dissolved in the water.
Below you will find a table showing a brief overview of the various
Methods. A detailed description of each can be found in the following sections.

| Method | Effectiveness | Optics | Costs | Suitable for compressed gas | Suitable for bio-CO2 |
| --- | --- | --- | --- | --- | --- |
| Bubbler / Diffuser | + | ++ | € | Yes | Yes |
| Inner reactor | + | + | € | Yes | Yes |
| Outer reactor | +++ | +++ | €€ | Yes | Yes |
| Inline diffuser | +++ | +++ | €€€ | Yes | TODO |

### Bubblers / Diffusers

Diffusers are the easiest and cheapest way to get CO<sub>2</sub> into the aquarium.
The diffuser is usually a kind of bell made of glass, the opening of which is covered with a membrane
This membrane is very fine-pored. When CO<sub>2</sub> with pressure behind the membrane
is introduced, the CO<sub>2</sub> flows through the microscopic
Openings in the membrane and countless fine bubbles arise in the aquarium water.

The resulting fine atomization of the CO<sub>2</sub> creates a large surface.
In this way, the CO<sub>2</sub> should be dissolved quite well in the aquarium water.

!!! The membrane of the diffuser will inevitably become clogged with algae and biofilm over time.
!!! For **cleaning** you can remove the diffuser from the aquarium and put it in a
!!! solution of water and chlorine for a few hours. Before using the diffuser in the aquarium again 
!!! it must be **rinsed thoroughly with clear water!**. Fish and especially shrimp are 
!!! quite sensitive to chlorine.

**Advantages** are the simple technique. The diffuser consists of one to a few components.
Accordingly, not much can break.
In addition, diffusers are available as glassware and are therefore quite attractive.

**A disadvantage** is certainly that the diffuser has to be placed in the aquarium and
possibly visible to the viewer. Not everyone likes this.
In addition, inner or outer reactors offer higher efficiency.


<!-- ### inner reactor
TO DO


### external reactors
TODO -->

### Inline Diffusers

Inline diffusers are looped into the outlet hose of the external filter. The principle is the same as
at the diffuser. A membrane ensures the fine atomization of the CO<sub>2</sub> and through flow of
water whirling the bubbles around, the inline diffuser achieves a very high efficiency.

The advantage is that the inline diffuser is not installed in the aquarium and is therefore invisible.
Due to the long contact of the CO<sub>2</sub> bubbles and the turbulence with the water,
it also achieves a high level of efficiency.

The higher costs, the more complex construction of an in-line diffuser and the more complex
installation can be seen as a disadvantage.
The device usually has to be removed from the filter circuit and be opened for cleaning.
Only then you can access to the membrane.
Furthermore, a very fine CO<sub>2</sub> mist often spreads in the aquarium. What efficiency
is beneficial, the appearance of the aquarium may be detrimental.


# The correct CO<sub>2</sub> concentration

If the CO<sub>2</sub> concentration in the water is too low, it has virtually no effect.
**Too high a concentration is fatal to living beings**.
It is therefore important to aim for an average value of 20 to 30 parts per million (ppm).

## Monitor the CO<sub>2</sub> concentration of the aquarium

![](dropchecker_crop.webp?cropZoom=700,700&classes=caption,w-3/4,sm:w-2/3,mx-auto "A dropchecker with light green checker liquid")

So-called CO<sub>2</sub> checkers can be used to monitor the CO<sub>2</sub> concentration. These are
mostly spherical or teardrop-shaped structures made of glass or plastic - so-called drop checkers.
A liquid is introduced into it that assumes certain colors for certain
CO<sub>2</sub> levels in the water. Blue usually signals too low
CO<sub>2</sub> level, green to light green are signals an optimum level. Yellow
indicates that the CO<sub>2</sub> concentration is too high.

The CO<sub>2</sub> Checker liquid is usually available in two versions. The one variant
changes from blue to green at a CO<sub>2</sub> concentration of 20 ppm, the other variant
at a concentration of 30 ppm. For heavily planted aquariums, a value of
30 ppm should be aimed for.

!!! Tip: Filling the CO<sub>2</sub> checker liquid into a drop checker
!!! is a bit cumbersome. A syringe with a slightly curved
!!! cannula can help fill in the liquid quickly and easily.

Be careful: The liquid reacts with a significant time delay to changes of
the CO<sub>2</sub> concentration in the water. Usually in the range of over 60 minutes.
So you cannot expect to increase the CO<sub>2</sub> inflow with the
needle valve and immediately see a color change in the liquid.
The inhabitants of the tank can "feel" the change before you can see it. So slowly adjust
the inflow via the needle valve over several hours and monitor the liquids color in case your
CO<sub>2</sub> levels are off.


## The bubble counter helps with the adjustment

A bubble counter is used for simple visual control of the inflowing
CO<sub>2</sub> amount when adjusting the needle valve.
The number of bubbles quickly tells you how strong when you turn the needle valve
you have changed the amount of CO<sub>2</sub> flowing in. This is how you get a better
sense of whether you've just made a big or small change.

However, a bubble counter does not give you any information about the current CO<sub>2</sub> content
of the water. The bubble counter is therefore **not** a substitute for continuous monitoring of the
CO<sub>2</sub> content of the water. It is only to be understood as an adjustment aid.
[See the previous section.](#monitor-the-co2-concentration-of-the-aquarium)

!!! Tip: Many diffusers fill up with water after a short time and you can
!!! also read a number of bubbles at this point. So a bubble counter is
!!! not always necessary.