---
title: 'CO2-Versorgung im Aquarium und Aquascape'
card:
    text: 'CO2-Versorgung ist elementar wichtig in stark bepflanzten Aquarien. Erfahre hier alles, was du zum Thema CO<sub>2</sub> wissen musst.'
    image: title.webp
metadata:
    description: 'CO2 ist ein wichtiger Grundbaustein für gesundes Pflanzenwachstum. Lies hier, was du zum thema CO2 im Aquascape und Aquarium wissen musst.'
    'og:description': 'CO2 ist ein wichtiger Grundbaustein für gesundes Pflanzenwachstum. Lies hier, was du zum thema CO2 im Aquascape und Aquarium wissen musst.'
    'og:image': 'https://scapershelper.de/user/pages/wissen/co2/sharepic.webp'
page-toc:
    active: true
intro: 'Eine CO<sub>2</sub>-Versorgung ist ein elementarer Bestandteil eines stark bepflanzten Aquariums. Sie sorgt für ein gesundes Pflanzenwachstum und und hilft somit Algen zu verhindern. Welche Arten von CO<sub>2</sub>-Anlagen es gibt, wie du CO<sub>2</sub> ins Aquarium einleitest und viele weitere Details erfährst du in diesem Artikel.'
---

# Was ist CO<sub>2</sub>?

Kohlenstoff ist ein Grundbaustein aller Lebenwesen inklusive  natürlich der Pflanzen.
Pflanzen nehmen Kohlenstoff in der Form von CO<sub>2</sub>, also Kohlenstoffdioxyd, auf.
CO<sub>2</sub> ist daher ein maßgeblicher Faktor für ein gesundes Pflanzenwachstum.
Neben CO<sub>2</sub> gehören Licht und Mikro-
sowie Makronährstoffen zu den Grundvoraussetzungen von Pflanzenwachstum.
Stehen diese „Zutaten” im richtigen Verhältnis, betreibt die Pflanze
[Photosynthese](https://de.wikipedia.org/wiki/Photosynthese) und bildet die zum
wachstum benötigten organischen Stoffe.

Falls du anspruchsvolle Pflanzen mit viel Licht pflegen willst, wird eine CO<sub>2</sub>-Zugabe dringend
empfohlen. **Durch die CO<sub>2</sub>-Versorgung stellst du sicher, dass deine Pflanzen keine
Mangelerscheinungen bekommen, gesund Wachsen und sich keine Algen breit machen können.**

CO<sub>2</sub>-Zugabe ist aber nicht immer nötig. In Aquarien mit nur wenig anspruchsvollen Pflanzen
und schwacher bis mittelstarker beleuchtetung kann auch auf die CO<sub>2</sub>-Zufuhr verzichtet werden.
Willst du solch ein ein Low-Tech-Aquascape aufsetzen,
[schaue hier vorbei für eine detaillierte Einkaufsliste](/wissen/low-budget-einkaufsliste),
die dir den Einstieg erleichtert. 

Wenn dich interessiert, wie Pflanzen mit und ohne Zugabe von CO<sub>2</sub> wachsen, kannst du das
in [diesem Video](https://youtu.be/tecnHZdBbyE?t=78) gut verfolgen. 
Dort siehst du die Entwicklung von zwei gleich bepflanzten Aquarien ohne und mit Zugabe von CO<sub>2</sub>.


# Überblick: Die verschiedenen CO<sub>2</sub>-Anlagen

Es gibt zwei etablierte Arten der CO<sub>2</sub>-Versorgung für dein Becken. Bei einer
**Bio-CO<sub>2</sub>Anlage** wird das CO<sub>2</sub> durch Gärung von Hefe in einer zuckrigen Lösung produziert.
Eine andere Möglichkeit ist eine **Druckgas-Anlage**. Hier wird das CO<sub>2</sub> industriell hergestellt
und kommt unter hohem Druck in einer Gasflasche daher.
Eine weitere, noch wenig verbreitete Möglichkeit, ist die **„Chemische CO<sub>2</sub>-Anlage”**.
Durch die Reaktion von Natron und Zitronensäure wird das CO<sub>2</sub> vor Ort hergestellt.
<!-- TODO: 1 Satz mit Details -->

Der Unterschied zwischen den verschiedenen Anlagen ist im Folgenden erläutert.

<!-- ## Bio-CO<sub>2</sub>
TODO 

Nachtabschaltung nicht möglich 

!!!! **Druckgas-CO<sub>2</sub> Einkaufsliste**
!!!! * Druckgas-Flasche 
!!!! * Druckminderer
!!!! * Nadelventil
!!!! * (Optional) Magnetventil und Zeitschaltuhr
-->


## Die Druckgas-CO<sub>2</sub>-Anlage

Mit einer Druckgasanlage bezeichnet man die in der Regel Kombination aus einer **Druckgas-CO<sub>2</sub>-Flasche**
in Größen von 500 g, 2 kg bis zu 10 kg, einem **Druckminderer** und einem **Nadelventil**.
Das CO<sub>2</sub> in der Flasche steht unter hohem Druck unter dem es in flüssiger Form vorliegt.
Lässt man das CO<sub>2</sub> aus der Flasche entweichen, bewirkt der niedrige
Umgebungsdruck den Übergang in den gasförmigen Zustand.
<!-- Typischerweise zwischen TODO und TODO bar.  -->

![CO2-Flasche mit Druckminderer, Flaschendruck- und Arbeitsdruckbarometer, Nadelventil und Magnetventil](co2flasche_nummeriert.webp?classes=caption "(1) Flaschenventil, (2) Arbeitsdruck, (3) Flaschendruck, (4) Arbeitsdruck-Einstellrad, (5) Nadelventil, (6) Magnetventil")

Dieser hohe Druck macht es unmöglich die Flasche direkt an einen Schlauch anzuschließen und 
das CO<sub>2</sub> über einen Diffusor oder Reaktor (unten mehr dazu) anzuschließen ins Aquarium zu leiten.
Ein Notwendiges Element der Druckgas-Anlage ist daher ein **Druckminderer**.
Er sorgt dafür, dass der hohe Flaschendruck von über 50 bar auf einen sogenannten Arbeitsdruck
von ca. 2 bar heruntergeregelt wird.

Der Druckminderer ist aber selbst noch zu ungenau, um die nötige Menge CO<sub>2</sub>
einzustellen, die dem Aquarium kontinuierlich zugeführt werden soll. Daher
ist nach dem Druckminderer ein **Nadelventil** angebracht. Mit diesem ist es durch
eine Stellschraube möglich, die eingeleitete CO<sub>2</sub>-Menge genau einzustellen.

[plugin:page-inject](/_app-cta)

### Nachtabschaltungsventil

Da die Pflanzen nur Photosynthese betreiben wenn ausreichend Licht zur Verfügung steht,
ist es Sinnvoll CO<sub>2</sub>-Druckgasanlagen mit einer **Nachtabschaltung**
auszustatten. <!-- TODO: Bild  --> Hierbei handelt es sich um ein elektrisch schaltbares Ventil,
ein sogenanntes Magnetventil, das üblicherweise direkt nach dem Druckminderer angebracht wird.

!! **Achtung:** Ein Nachtabschaltungsventil ersetzt nicht das Rückschlagventil.
!! Magnetventil, Nadelventil und Druckminderer sind nur für das Führen von Gasen
!! ausgelegt. Ein Eindringen von Wasser kann sie zerstören.

Mit einer Zeitschaltuhr wird das Ventil nur geöffnet, sofern auch die Beleuchtung eingeschaltet ist.
Alternativ kann das Ventil auch bereits 30 Minuten bis eine Stunde vor der Beleuchtung eingeschaltet werden
sowie 30 Minuten bis eine Stunde bevor die Beleuchtung ausgeschaltet werden.
So wird sichergestellt, dass das Aquarienwasser nur dann mit CO<sub>2</sub>
angereichert wird, wenn es tatsächlich notwendig ist. Bei achtstündiger Einschaltzeit hält die
CO<sub>2</sub>-Flasche dann dreimal so lange wie ohne Nachtabschaltung.

<!-- Welche Flaschengröße für welche Beckengröße? -->

!!! **Druckgas-CO<sub>2</sub> Komponenten zusammengefasst**
!!! * **Druckgas-Flasche** 
!!! * **Druckminderer** – Regelt den Flaschendruck auf den Arbeitsdruck
!!! * **Nadelventil** – Zur Feinjustage der ins Aquarium geleiteten CO<sub>2</sub>-Menge
!!! * **Blasenzähler** – Zur Hilfe bei der Justage der eingeleiteten CO<sub>2</sub>-Menge
!!! * **Rückschlagventil** – Zum Schutz der Ventile und Druckminderer
!!! * (Optional) **Magnetventil und Zeitschaltuhr** – Zum einsparen von CO<sub>2</sub>

### Leere Druckgasflasche befüllen lassen

Ist die CO<sub>2</sub>-Flasche leer, so muss sie wieder befüllt werden. Dies kann üblicherweise
an drei Orten geschehen: Beim Gashandel, beim Getränkehandel oder beim Zoohandel. Der **Gashhandel**
hat preislich vermutlich das beste Angebot. Es ist allerdings nicht unüblich,
dass deine Flasche gegen eine andere, bereits befüllte Flasche ausgetauscht wird und die leere 
Flasche bei der Händler:in bleibt Sie wird dort befüllt und zurück in den Kreislauf gegeben.
Der Vorteil ist, dass du so immer eine Flasche mit gültigem TÜV bekommst.
Es kann aber sein, dass du eine Flasche mit anderen Maßen bekommst und
somit möglicherweise Platzprobleme im Unterschrank entstehen.

Eine andere Option sind **Getränkehändler**. Viele Getränkehändler haben die Möglichkeit
deine Flasche zu füllen. Hier ist es eher unüblich, dass deine Flasche gegen eine
andere getauscht wird. Normalerweise gehst du also mit deiner eigenen, frisch befüllten
Flasche nach hause.

Beim **Zoohandel** kannst du deine Flasche auch füllen lassen. Hier bekommst du immer deine eigene Flasche
aufgefüllt. Beim Zoohandel wird die Füllung vermutlich am teuersten sein.
Als Beispiel: Eine Füllung meiner 2 kg CO<sub>2</sub>-Flasche beim lokalen Zoohandel kostet 17,99 €.

Prüfe nach dem Wechsel der Flasche auf jeden Fall, ob Gas austritt. Das kannst du zum Beispiel mit
speziellem Lecksuchspray oder ganz einfach mit Spüli. Mische dazu eine geringe Menge Wasser
mit etwas Spülmittel. Mit dem Finger oder einer Spritze kannst du die Mischung nun auf das
Gewinde aufbringen. Siehst du es irgendwo schäumen oder blubbern hast du ein Leck und solltest
die Verschraubung noch prüfen und gegebenenfalls fester ziehen.

### Nutzung von Sodastream CO<sub>2</sub>-Flaschen am Aquarium

Sodastream Flaschen können mittels eines Adapters an das Gewinde der handelsüblichen
Aquaristik-Druckminderer angeschlossen werden. Die Adapter finden sich im Onlinehandel,
beispielsweise bei eBay oder Amazon.

Die Nutzung von sodastream CO<sub>2</sub>-Flaschen bietet den Vorteil, dass man
leere Flaschen in vielen Supermärkten und Drogeriemärkten gegen volle tauschen lassen kann.
Der Weg zum Supermarkt ist für die meisten vermutlich deutlich kürzer als der, 
zur nächsten Zoohandlung oder dem Gashändler. Ein deutlicher Komfortgewinn also.

Die Sodastream flaschen enthalten aber lediglich 425 g CO<sub>2</sub>. Die Kosten sind 
im Vergleich zur Füllung einer größeren Flasche beim Gas- oder Zoohandel also
deutlich höher.


## Chemische CO<sub>2</sub>-Anlage</sub>

Eine „Chemische CO<sub>2</sub>-Anlage” basiert auf der Reaktion von 
Natron und Zitronensäure Substanzen miteinander.

<!-- TODO: Bilder -->

Die Anlage besteht aus einem Edelstahlzylinder sowie einem
Druckminderer, einem Nadelventil, gegebenenfalls einem Magnetventil und
meist aus einem Blasenzähler. Der Druckminderer lässt sich vom Edelstahlzylinder
abschrauben und man erhält durch die so freigegebene Öffnung Zugang zum Inneren des Zylinders.
Durch diese können die Chemikalien eingefüllt werden.

!! Bitte halte dich bezüglich der Mengen der Chemikalien und die Reihenfolge,
!! in der du sie in den Zylinder füllst, unbedingt an die Angaben des Herstellers.

In dem Edelstahlzylinder mischt man die Chemikalien Natron, Zitronensäure und Wasser
in der Regel in einem Verhältnis von 1:1:2. Die Reaktion beginnt sofort und es wird CO<sub>2</sub>
freigesetzt. Nun wird der Druckminderer mitsamt des Nadelventils sowie der eventuellen
Nachtabschaltung auf den Zylinder aufgeschraubt.

Der Vorteil gegenüber Bio-CO<sub>2</sub> ist, dass diese Reaktion kontinuierlich läuft
und nicht temperaturabhängig ist. Auch ist die Reaktion unabhängig vom Druck im Behälter.
Das bedeutet, dass eine **Nachtabschaltung möglich ist**.

Für ein 30-40 Liter Becken hält eine Menge von jeweils 400 g Natron und Zitronensäure
circa 100 Tage [laut diesem Video vom AquaOwner](https://www.youtube.com/watch?v=8Wqjlauat0s).
Kauft man Natron und Zitronensäure in Mengen von einem Kilogramm,
belaufen sich die Kosten für eine Füllung auf circa 8 Euro. Eine echte Alternative
zur Druckgasanlage also.

<!-- TODO: Optik gut! -->

<!-- Zusammenfassung mit Tabelle -->

# CO<sub>2</sub> ins Aquarium einleiten

Ziel ist es, das CO<sub>2</sub> im Aquarienwasser zu lösen. nur so steht es den
Pflanzen zur Verfügung. Um dies möglichst effizient zu gestalten, sollte die Fläche
des CO<sub>2</sub> möglichst groß sein, wenn es am Wasser vorbei geführt wird. Dies kann
beispielsweise erreicht werden, indem das CO<sub>2</sub> in möglichst kleine Bläschen zerstäubt wird.

Die unten vorgestellten Methoden haben alle ihre Vor- und Nachteile bezüglich Optik
oder der Effektivität, mit der das CO<sub>2</sub> im Wasser gelöst wird.
Nachfolgend findet ihr eine Tabelle zur kurzen Übersicht über die verschiedenen
Methoden. Details findet ihr in den nachfolgenden Abschnitten.


|Methode|Effektivität|Optik|Kosten|Druckgas-geeignet|Bio-CO2-geeignet|
|--- |--- |--- |--- |--- |--- |
|Sprudler / Diffusor|+|++|€|Ja|Ja|
|Innenreaktor|+|+|€|Ja|Ja|
|Außenreaktor|+++|+++|€€|Ja|Ja|
|Inline-Diffusor|+++|+++|€€€|Ja|TODO|


### Sprudler / Diffusoren

Diffusoren sind die einfachste und kostengünstigste Methode CO<sub>2</sub> ins Aquarium
einzuleiten. Der Diffusor ist üblicherweise eine art Glocke aus Glas, deren Öffnung mit einer Membran
versehen ist. Diese Membran ist sehr feinporig. Wenn CO<sub>2</sub> mit Druck hinter der Membran
eingeleitet wird, strömt das CO<sub>2</sub> durch die mikroskopisch kleinen
Öffnungen in der Membran aus und es entstehen unzählige feine Blasen im Aquarienwasser.

Durch die so entstehende feine Zerstäubung des CO<sub>2</sub> wird eine große Oberfläche erzielt.
So soll das CO<sub>2</sub> möglichst gut im Aquarienwasser gelöst werden.

!!! Die Membran des Diffusors wird sich mit der Zeit unweigerlich mit Algen und Biofilm zusetzen.
!!! Zur **Reinigung** kann man den Diffusor aus dem Aquarium entfernen und einige Stunden in eine 
!!! Lösung aus Wasser und Chlorreiniger legen. Bevor der Diffusor wieder im Aquarium verwendet wird,
!!! muss er **unbedingt gründlich mit klarem Wasser ab- und durchgespült werden!**.

**Vorteile** sind die einfache Technik. Der Diffusor besteht aus einem bis wenigen Bauteilen.
Dementsprechend kann nicht viel kaputt gehen.
Außerdem sind Diffusoren als Glasware erhältlich und dementsprechend recht ansehnlich.

**Nachteilig** ist sicherlich, dass der Diffusor im Aquarium platziert werden muss und
gegebenenfalls für den Betrachter sichtbar ist. Dies gefällt nicht jedem.
Außerdem bieten Innen- oder Außenreaktor eine höhere Effizienz.


<!-- ### Innenreaktor
TODO


### Außenreaktoren
TODO -->


### Inline-Diffusoren

Inline-Diffusoren werden in den Auslassschlauch des Außenfilters eingeschleift. Das Prinzip ist das Gleiche wie
beim Diffusor. Eine Membran sorgt für die feine Zerstäubung des CO<sub>2</sub> und durch die zusätzliche
Verwirbelung der Bläschen erreicht der Inline-Diffusor eine sehr hohe Effizienz.

Vorteilig ist, dass der Inline-Diffusor nicht im Aquarium angebracht wird und so unsichtbar ist.
Durch den langen Kontakt und die Verwirbelung mit dem Wasser erreicht er außerdem eine hohe Effizienz.

Als Nachteil können die höheren Kosten betrachtet werden, sowie den aufwändigeren Einbau
und die komplexere Konstruktion eines Inline-Diffusors.
Für die Reinigung muss das Gerät üblicherweise aus dem Filterkreislauf ausgebaut
und geöffnet werden. Erst dann hat man Zugriff auf die Membran.
Weiterhin entsteht meist ein sehr feiner CO<sub>2</sub>-Nebel im Aquarium. Was für die Effizienz
förderlich ist, ist der Ansehnlichkeit des Aquariums unter Umständen abträglich.


# Der richtige CO<sub>2</sub>-Gehalt des Wassers

Ein zu niedriger CO<sub>2</sub>-Gehalt des Wassers ist quasi wirkungslos.
**Ein zu hoher Gehalt ist tödlich für Lebewesen**.
Es gilt daher einen Mittelwert von 20 bis 30 parts per million (abgekürzt: ppm) anzustreben.


## Den CO<sub>2</sub>-Gehalt des Aquariums überwachen

![](dropchecker_crop.webp?cropZoom=700,700&classes=caption,w-3/4,sm:w-2/3,mx-auto "Ein Dropchecker mit hellgrüner Checker-Flüssigkeit")

Um diesen Wert zu überwachen bieten sich sogenannte CO<sub>2</sub>-Checker an. <!-- TODO: bild --> Dies sind
meist kugel- oder tropfenförmige Gebilde aus Glas oder Kunststoff - sogneannte Drop-Checker.
In sie wird eine Flüssigkeit eingebracht, die bestimmte Farben annimt für bestimmte 
CO<sub>2</sub>-Mengen im Wasser. Blau bedeutet üblicherweise ein zu niedriger
CO<sub>2</sub>-Gehalt, grün bis hellgrün sind das Optimum und gelb
signalisiert einen zu hohen CO<sub>2</sub>-Gehalt.

Die CO<sub>2</sub>-Checker-Flüssigkeit gibt es in der Regel in zwei Varianten. Die eine Variante
schlägt bei einem CO<sub>2</sub>-Gehalt von 20 ppm von Blau auf Grün um, die andere Variante
bei einem Gehalt von 30 ppm. Für stark bepflanzte Aquarien kann ohne Bedenken ein Wert von
30 ppm angestrebt werden.

!!! Tipp: Das Nachfüllen der CO<sub>2</sub>-Checker-Flüssigkeit in einen Drop-Checker 
!!! ist ohne Hilfsmittel etwas umständlich. Eine Spritze mit einer leicht
!!! gebogenen Kanüle kann helfen die Flüssigkeit schnell und einfach einzufüllen.

Die Flüssigkeit reagiert mit einer deutlichen Zeitverzögerung auf Änderungen
des CO<sub>2</sub>-Gehalts des Wassers. Üblicherweise im Bereich von über 60 Minuten.
Du kannst also nicht erwarten, dass du am Nadelventil der CO<sub>2</sub>-Anlage
drehst und sofort eine Farbveränderung der Flüssigkeit siehst.


## Der Blasenzähler hilft bei der Justage

Ein Blasenzähler dient zur einfachen optischen Kontrolle der Einströmenden
CO<sub>2</sub>-Menge beim Verstellen des Nadelventils.
Die Anzahl der Blasen verrät dir beim drehen am Nadelventil schnell, wie stark
du die einströmende Menge CO<sub>2</sub> verändert hast. So bekommst du ein besseres
Gefühl dafür, ob du gerade eine große oder kleine Veränderung vorgenommen hast.

Ein Blasenzähler gibt dir aber keine Auskunft über den aktuellen CO<sub>2</sub>-Gehalt
des Wassers. Der Blasenzähler ist daher **kein** Ersatz für eine kontinuierliche Überwachung des 
CO<sub>2</sub>-Gehalts des Wassers. Er ist nur als Einstellhilfe zu verstehen.
[Siehe dazu den vorigen Abschnitt.](#den-co2-gehalt-des-aquariums-ueberwachen)

!!! Tipp: Viele Diffusoren füllen sich nach kurzer Zeit mit wasser und man kann
!!! auch an dieser Stelle eine Blasenzahl ablesen. Ein Blasenzahler ist also
!!! nicht immer nötig.