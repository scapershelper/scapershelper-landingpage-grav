---
title: 'Low-Budget Aquascaping Einkaufsvorschläge'
card:
    text: 'Der Einstieg ins Aquascaping muss nicht sehr teuer sein. Erfahre hier, wie du mit 200 € ein tolles Low-Tech Becken aufbaust.'
    image: empty_scape_with_products.webp
media_order: 'empty_scape_with_products.webp,scape_after_filling.webp'
page-toc:
    active: true
    depth: 3
intro: 'Der Einstieg ins Aquascaping muss nicht sehr teuer sein. Ab 200 € kannst du dir einen tollen Low-Tech Cube aufbauen an dem du lange Freude haben wirst. Die Auswahl der Pflanzen ist natürlich limitiert aber gerade unter schwierigen Bedingungen ein gut funktionierendes und schönes Scape zu erschaffen ist ein tolles Gefühl.'
---

# Werkzeuge, Zubehör und Dünger

Um ein wenig an Zubehör wirst du kaum herum kommen. Sand-Flattener und Pinsel sind – insbesondere wenn du Geld sparen möchtest – absolut unnötig. Ein paar Dinge sind jedoch essenziell und machen das Leben deutlich einfacher. Außerdem wirst du auch um zwei bis drei Dünger nicht herum kommen um das Aquascape auf Dauer algenfrei halten zu können. Empfehlenswert ist folgendes Zubehör:

- Ein Schwamm zum Scheiben putzen
- Eine Zahnbürste um die Kanten des Aquascapes und das Hardscape zu reinigen
- Ggf. eine Nagelbürste oder größere Bürste um große Steine und Wurzeln zu reinigen
- Eine Pinzette hilft ungemein beim Einpflanzen von Stengelpflanzen und Bodendeckern. Es muss nicht die Pinzette aus dem Aquariengeschäft sein. Die vom Drogeriemarkt ist genauso gut

Auch wenn das benutzte Soil einige Wochen genügend Nitrat ans Wasser abgibt, so brauchst du vermutlich langfristig einen Mikronährstoffdünger. Ein Eisenvolldünger deckt den Mikronährstoffbedarf der Pflanzen ab und ist zumindest empfehlenswert. Ggf. brauchst du auch einen NPK Dünger.

# Aquascaping mit kleinem Budget

![empty_scape_with_products](empty_scape_with_products.webp?class=caption,mx-auto,w-full,md:w-2/3 "Dennerle 35l Cube mit Zubehör")

Auch mit einem kleinen Budget kannst du mit dem Aquascaping beginnen allerdings mit Einschränkungen. Auf ein teures CO2-System muss verzichtet werden. Eine RGB-LED-beleuchtung passt auch nicht ins Budget, ist aber nicht schlimm, eine reinweiße LED-Beleuchtung funktioniert auch wunderbar. Fische und Garnelen werden zunächst nicht eingesetzt. Das spart zusätzlich Geld und außerdem kann dadurch auf ein Heizer sowie auf eine – je nach Wasserwertigen nötige – Umkehrosmoseanlage verzichtet werden. 

Als Aquarium kommt ein kleiner Cube zum Einsatz. Diese sind recht günstig und ein toller Hingucker. Auf einen Unterschrank wird verzichtet. Die vorgeschlagenenen Aquarien sind recht klein und daher sollten sie auf eienr schon vorhandenen Kommode oder einem Nachttisch wunderbar aufstellbar sein.

Wenn wieder Geld in der Kasse ist, kannst du dir überlegen, ob du Garnelen einsetzen möchtest. Spartipp: Garnelen gibt es oft von Hobbyzüchtern bei eBay Kleinanzeigen schon ab 1 € pro Stück.

!!! Achte unbedingt darauf, dass die Wasserwerte und Temperatur angemessen für deinen geplanten Besatz sind. 
!!! 
!!! Für Garnelen musst du sicherstellen, dass sie nicht in den Filter eingesaugt werden. Du kannst dazu beispielsweise einen groben Filterschwamm über den Filtereinlauf stülpen oder ein dafür vorgesehenen „Filter-Guard” aus Metallnetz kaufen.
!!! Ggf. sind außerdem die Anschaffung eines Heizers oder größere Anschaffungen wie eine Umkehrosmoseanlage nötig.

Bei der Wahl der Pflanzen solltest du unbedingt darauf achten, dass diese niedrige Ansprüche haben. Moose, Farne und Anubien eignen sich hier hervorragend. Pflanzen mit hohen Ansprüchen werden ohne CO2-Versorgung und den großen Lichtmengen eines High-Tech Beckens mit großer Wahrscheinlichkeit verkümmern. Spare dir das Geld.

## Vorschlag 1: 30 l Cube

Vorschlag 1 bietet schon für 200 € einen Einstieg ins Aquascaping. Mit einem qualitativ hochwertigen Cube von Dennerle mit den Maßen 30x30x35 cm (BxTxH), einem bewährten Filter von Eheim und einer dimmbaren LED-Leuchte von Chihiros wirst du lange Freude haben und vielleicht bald Lust auf mehr bekommen.

![30 l Dennerle Cube gleich nach dem ersten Befüllen](scape_after_filling.webp?classes=mx-auto,w-full,md:w-2/3,caption "30 l Dennerle Cube gleich nach dem ersten Befüllen")

Das Bild zeigt meinen Low-Tech Cube direkt nach dem befüllen. Ich habe folgende Pflanzen verwendet: Bolbitis heudelotii (Kongofarn), Hydrocotyle tripartita (Dreiteiliger Wassernabel), Cryptocoryne wendtii "braun" (Wendts Wasserkelch, braun), Microsorum pteropus 'Narrow' (Schmalblättriger Javafarn), Limnophila sessiliflora (Blütenstielloser Sumpffreund), 
Vesicularia ferriei 'Weeping Moss'. Aus einem anderen Becken habe ich außerdem ein paar Blätter Phyllanthus fluitans (Schwimm-Wolfsmilch) sowie ein paar Stängel Rotala Indica, Staurogyne Repens sowie Alternanthera reineckii eingesetzt.

### Die Einkaufsliste

| Produkt | Preis |
|---|---|
| Dennerle 35 l Cube | 45 € |
| _Alternativ: Glasgarten Mini M_ | _50 €_ |
| Chihiros LED System Serie C C301 | 45 € |
| Eheim Classic 150 | 50 € |
| Tropica Aquarium Soil 3 l | 17 € |
| Pflanzen ca. 6 Stück | 40 € |
| Lavamulch aus dem Baumarkt | 6 €  |
| Hardscape selbst sammeln |  |
| **Summe** | **203 €** |

## Vorschlag 2: Scaper's Tank oder 45 P

Bei diesem Vorschlag bekommst du für 230 € hochwertige Komponenten für ein Scape in klassischen Scape-Maßen, sprich flacher und tiefer als klassische Aquarien. Mit dem Scaper's Tank von Dennerle gibt's wieder ein hochwertiges Glas mit gerundeten Kanten an der Front. Gefallen dir die abgerundeten Kanten nicht, kannst du das Becken auch einfach so aufstellen, dass die Rundungen nach hinten zeigen. Alternativ kannst du auch auf ein 45 P von GlasGarten in Weißglasausführung zurückgreifen.

Anstatt der kleinen Anklemmleuchte aus Vorschlag 1 wird bei diesem Vorschlag eine LED-Balkenleuchte verwendet. Natürlich kann alternativ auch die Chihiros C301 aus Vorschlag 1 verwendet werden.

### Die Einkaufsliste

| Produkt | Preis |
|---|---|
| Dennerle Scaper's Tank 35 l | 70 € |
| _Alternativ: GlasGarten ECO-Line 45 P_ | _60 €_ |
| Chihiros LED System Serie A A401 | 53 € |
| _Alternativ: Chihiros LED System Serie C C301_ | _45 €_ |
| Eheim Classic 150 | 50 € |
| Tropica Aquarium Soil 3 l | 17 € |
| Pflanzen ca. 6 Stück | 40 € |
| Lavamulch aus dem Baumarkt | 6 €  |
| Hardscape selbst sammeln |  |
| **Summe** | **236 €** |

# Weiterführende Links
- [Low-Budget Aquascape Playlist von AquaOwner](https://www.youtube.com/playlist?list=PLSWOo5dsenaDhEDGabl3171KSAW5jxAlj)