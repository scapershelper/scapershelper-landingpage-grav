---
title: 'Low-budget aquascaping shopping list and guide'
card:
    text: 'Getting started with aquascaping doesn''t have to be very expensive. Find out here how you can build a great low-tech scape with 200 €.'
    image: empty_scape_with_products.webp
media_order: 'empty_scape_with_products.webp,scape_after_filling.webp'
page-toc:
    active: true
    depth: 3
intro: 'Getting started with aquascaping doesn''t have to be very expensive. From 200 € you can build a great low-tech cube that you will enjoy for a long time. The selection of plants is of course limited, but creating a well-functioning and beautiful scape, especially under difficult conditions, is a great feeling.'
---

# Tools, accessories and fertilizers

You will hardly get around a little accessories. Sand flatteners and brushes are absolutely unnecessary, especially if you want to save money. However, a few things are essential and make life a lot easier. In addition, you will not be able to avoid two to three fertilizers in order to be able to keep the aquascape free of algae in the long term. The following accessories are recommended:

- A sponge to clean the windows
- A toothbrush to clean the edges of the aquascape and the hardscape
- If necessary, a nail brush or larger brush to clean large stones and roots
- Tweezers are a great help when planting stem plants and ground cover. It doesn't have to be the tweezers from the aquarium store. The one from the drugstore is just as good

Even if the used soil releases enough nitrate into the water for a few weeks, you probably need a micronutrient fertilizer in the long run. A complete iron fertilizer covers the micronutrient requirements of the plants and is at least recommended. You may also need an NPK fertilizer.

# Aquascaping on a budget

![Dennerle 35l Cube with accessories](empty_scape_with_products.webp?class=caption,mx-auto,w-full,md:w-2/3 "Dennerle 35l Cube with accessories")

With some restrictions you can start aquascaping even with a small budget. You have to do without an expensive CO2 system. RGB LED lighting does not fit into the budget either, but it is not a problem, pure white LED lighting also works wonderfully. Fish and shrimp are initially not used. This also saves money and also means that there is no need for a heater or a reverse osmosis system, which may be necessary depending on the water quality.

A small cube is used as an aquarium. These are quite cheap and a great eye-catcher. There is no base cabinet. The proposed aquariums are quite small and should therefore be easy to set up on an existing chest of drawers or a bedside table.

When there is money in the till again, you can consider whether you want to get shrimp. Saving tip: Shrimp are often available from hobby growers on online classified ad platforms like Gumtree for as little as € 1 each.

!!! Make sure that the water parameters and temperature are appropriate for your planned stocking. 
!!! 
!!! For shrimp you should make sure they can't be sucked in into the filter. You can do so by fitting a filter sponge over the inflow or get a filter guard suited for shrimp.
!!! Furthermore it may be necessary to get a heater or invest even more money in things such as a reverse osmosis system.

When choosing plants, you should make sure that they have low requirements. Mosses, ferns and anubias are ideal here. Plants with high demands are very likely to wither away without a CO2 supply and the large amounts of light of a high-tech tank. Save the money.

## Suggestion 1: 30 l cube

Proposal 1 offers an introduction to aquascaping for just € 200. With a high-quality cube from Dennerle with the dimensions 30x30x35 cm (WxDxH), a tried and tested filter from Eheim and a dimmable LED light from Chihiros, you will enjoy it for a long time and maybe soon you will want more.

![30 l Dennerle Cube immediately after the first filling](scape_after_filling.webp?classes=mx-auto,w-full,md:w-2/3,caption "30 l Dennerle Cube after the first filling")

The picture shows my low-tech cube right after filling it. I used the following plants: Bolbitis heudelotii (Congo fern), Hydrocotyle tripartita, Cryptocoryne wendtii "brown" (Wendt's water trumpet), Microsorum pteropus 'Narrow' (narrow-leaved Java fern), Limnophila sessiliflora (dwarf ambulia), Vesicularia ferriei 'Weeping Moss'. I also used a few leaves of Phyllanthus fluitans (red root floater or apple duckweed) and a few stems of Rotala Indica, Staurogyne Repens and Alternanthera reineckii from another tank.

### The shopping list

| Product | Price |
| --- | --- |
| Dennerle 35 l Cube | 45 € |
| _Alternative: Glass garden Mini M_ | _50 € _ |
| Chihiros LED System Series C C301 | 45 € |
| Eheim Classic 150 | 50 € |
| Tropica Aquarium Soil 3 l | 17 € |
| Plants approx. 6 pieces | 40 € |
| Lava mulch from the hardware store | 6 € |
| Collect hardscape yourself | |
| **Sum** | **203 €** |

## Suggestion 2: Scaper's Tank or 45 P

With this proposal, you get high-quality components for a scape in classic dimensions, i.e. shallower and deeper than classic aquariums. With the Scaper's Tank from Dennerle, there is again a high-quality glass with rounded edges on the front. If you don't like the rounded edges, you can simply set up the pool so that the curves point backwards. Alternatively, you can use a 45 P from GlasGarten in white glass.

Instead of the small clip-on lamp from proposal 1, a LED bar is used in this proposal. Of course, the Chihiros C301 from suggestion 1 can also be used as an alternative.

### The shopping list

| Product | Price |
| --- | --- |
| Dennerle Scaper's Tank 35 l | € 70 |
| _Alternative: GlasGarten ECO-Line 45 P_ | _60 € _ |
| Chihiros LED System Serie A A401 | € 53 |
| _Alternative: Chihiros LED System Series C C301_ | _45 € _ |
| Eheim Classic 150 | 50 € |
| Tropica Aquarium Soil 3 l | 17 € |
| Plants approx. 6 pieces | 40 € |
| Lava mulch from the hardware store | 6 € |
| Collect hardscape yourself | |
| **Sum** | **236 €** |

# Related Links
- [Low-Budget Aquascape Playlist by AquaOwner](https://www.youtube.com/playlist?list=PLSWOo5dsenaDhEDGabl3171KSAW5jxAlj)