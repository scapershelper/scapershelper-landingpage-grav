---
title: 'Kompatible Produkte und technische Details'
media_order: DSCF1233_sm.jpg
page-toc:
    active: true
card:
    text: 'Erfahre hier welche Produkte kompatibel sind und welche Belegungen die Anschlüsse haben.'
    image: DSCF1233_sm.jpg
---

# Dimmung von LED Leuchten

![](DSCF1233_sm.jpg?cropResize=700,700) Mit sechs Dimmungs-Kanälen kannst du zum Beispiel deine AquaGrow oder Chihiros RGB bzw. WRGB Leuchten steuern. So kannst du Sonnenauf- und untergänge, Mondlicht und Gewitter simulieren und vieles mehr.

## Kompatible LED Leuchten

* Die gesamte AquaGrow UNIQ Serie
* Die gesamte AquaGrow Solid LS Serie
* Die gesamte AquaGrow Solid LB Serie inklusive der Solid Plus LB Serie
* Die gesamte AquaGrow Solid AL Serie
* Chihiros Serie WRGB
* Chihiros Serie WRGB II
* Chihiros Serie A
* Chihiros Serie A II
* Kompatibel sind weiterhin **alle LED Leuchten deren Kanäle auf GND-Seite geschaltet werden** … TODO. Bitte die unbedingt die Maximalbelastung beachten.

## Anschlussbelegung
TODO


## Maximalbelastung

|  | Wert | Einheit |
|-|-:|-|
| Maximalspannung | 45 | V |
| Maximalstrom | TODO | A |

# 0-10 Volt-Anschlüsse

Die 0-10 Volt Anschlüsse lassen dich Leuchten und Netzteile mit entsprechender Schnittstelle ansteuern. Weiterhin können Strömungspumpen geregelt werden.

Der 0-10 V Anschluss kann nicht nur 0-10 V sondern auch 1-10 V ausgeben und ist somit auch mit allen 1-10 V Produkten kompatibel.

## Kompatible Leuchten und Netzteile

* Kessil XYZ
* Die gesamte Meanwell TODO Serie

## Kompatible Strömungspumpen

* Tunze XYZ

## Anschlussbelegung

| Kontakt | Belegung |
|---------|----------|
| Tip     | 3,3 V |
| Ring    | 0 … 10 V |
| Sleeve  | GND |

## Maximalbelastung

|  | Wert | Einheit |
|-|-:|-|
| Maximalstrom | TODO | A |

# Steckdosen- und Relais-Anschlüsse

Die Steckdosen- und Relais-Anschlüsse liefern je 12 V und bis max TODO A.

## Kompatible Produkte

* GHL STDL 4-4
* Alle 12 V-Relais. Bitte die Strombelastbarkeit des Anschluss beachten (s.u.)

## Anschlussbelegung

TODO

## Maximalbelastung

|  | Wert | Einheit |
|-|-:|-|
| Spannung | 12 | V |
| Maximalstrom | TODO | A |

# Düngepumpenanschlüsse

## Anschlussbelegung

# Universalanschlüsse

An den Universalanschlüssen (GPIO = General Purpose Input/Outpu) können Sensoren sowie Aktoren angeschlossen werden. TODO

## Kompatible Produkte

![](durchflusssensor_sm.jpg?cropResize=700,700&classes=caption "Ein Durchflusssensor")

![](fuellstand_ir_sm.jpg?cropResize=700,700&classes=caption "Ein IR-Pegelsensor")

![](pegelsensor.jpg?cropResize=700,700&classes=caption "Ein mechanischer Pegelsensor")

![](leakage_sm.jpg?cropResize=700,700&classes=caption "Ein Lecksensor")

## Anschlussbelegung

| Kontakt | Belegung |
|---------|----------|
| Tip     | 3,3 V    |
| Ring    | GPIO   |
| Sleeve  | GND      |

## Maximalbelastung

|  | Wert | Einheit |
|-|-:|-|
| Spannung | TODO … TODO | V |
| Maximalstrom als Senke | TODO | A |
| Maximalstrom als Quelle | TODO | A |

# pH-Sonden-Anschlüsse

## Kompatible Sonden

Alle handelsüblichen pH-Sonden mit BNC-Anschluss.

![](ph_sonde_bearbeitet_sm.jpg?cropResize=700,700&class=caption "Eine pH-Sonde mit angebrachtem Schutzbehälter sowie dem typischen BNC-Anschluss")

# Temperatursensoranschlüsse



## Kompatible Sensoren

Alle [DS18B20 Temperatursensoren](https://www.maximintegrated.com/en/products/sensors/DS18B20.html) des Herstellers maxim integrated und deren Klone. Bitte die Anschlussbelegung beachten.

## Anschlussbelegung

| Kontakt | Belegung |
|---------|----------|
| Tip     | 3,3 V    |
| Ring    | 1-Wire   |
| Sleeve  | GND      |

# Erweiterungsanschlüsse

Der Erweiterungsanschluss basiert auf einem CAN-Bus.

Die Erweiterungsanschlüsse sind für zukünftige Produkte vorgesehen und noch nicht in Nutzung. 

## Anschlussbelegung

| Kontakt | Belegung |
|---------|----------|
| 1 (VBUS) | 12 V |
| 2 (D-) | CAN L |
| 3 (D+) | CAN H |
| 4 | GND |