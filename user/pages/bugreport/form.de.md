---
title: Bugreport
form:
    name: contact-form
    fields:
        - name: email
          label: Email
          placeholder: Deine E-Mail Adresse für Rückfragen
          type: email
          validate:
            required: true

        - name: text
          label: Text
          type: textarea
          rows: 10
          validate:
            required: true

    buttons:
        - type: submit
          value: Submit

    process:
        - email:
            from: "{{ config.plugins.email.from }}"
            to:
              - "bugreport@scapershelper.de"
            subject: "[Feedback] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
            body: "{% include 'forms/data.txt.twig' %}"
        - message: Thank you for your feedback!
        - display: thankyou
---

Vielen Dank für die Meldung eines Problems oder Fehlers! 
Bitte gib so viele Informationen wie an sodass wir das Problem nachstellen können.
Zum Beispiel:

* Die App-Version (im unteren Bereich des Einstellungsmenüs zu finden)
* Der Gerätehersteller und -modell
* Die Android-Version des Geräts
* Eine Beschreibung, wie das Problem nachzustellen ist oder wie du es entdeckt hast

Vielen Dank!
