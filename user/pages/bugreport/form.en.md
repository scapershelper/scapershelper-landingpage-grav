---
title: Bugreport
form:
    name: contact-form
    fields:
        - name: email
          label: Email
          placeholder: Your email adress for possible questions
          type: email
          validate:
            required: true

        - name: text
          label: Text
          type: textarea
          rows: 10
          validate:
            required: true

    buttons:
        - type: submit
          value: Submit

    process:
        - email:
            from: "{{ config.plugins.email.from }}"
            to:
              - "bugreport@scapershelper.de"
            subject: "Bugreport"
            body: "{% include 'forms/data.html.twig' %}"
        - message: Thank you for your feedback!
        - display: thankyou
---

Thanks for reporting a problem or bug! Please include as much information as possible for us to
be able to reproduce the problem you're facing. For instance:

* The apps version (found in the bottom of the settings menu)
* Your device manufacturer and model
* The Android version your device is running
* A description of how to reproduce the probelem

Thank you so much!
