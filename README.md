# The scapershelper.de Landingpage

This is the [grav](https://getgrav.org) site used for [scapershelper.de](https://scapershelper.de)

This site is based on a grav installation. In this repo only the `user` folder is checked in. If you want to install this site, you have to install a grav locally and use the `user` folder from this repo.
